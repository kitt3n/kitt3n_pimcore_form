<?php

namespace KITT3N\Pimcore\FormBundle\Interfaces;

interface SharedPropertiesInterface{

    /**
     * @return string
     */
    public function sClass() : string;

    /**
     * @return string
     */
    public function sPropertyPrefix() : string;

    /**
     * @return string
     */
    public function sParentMembersGroup() : string;

    /**
     * @return string
     */
    public function sMembersGroup() : string;

}
