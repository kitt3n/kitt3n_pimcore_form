<?php

namespace KITT3N\Pimcore\FormBundle\LinkGenerator;

use KITT3N\Pimcore\FormBundle\Interfaces\SharedPropertiesInterface;
use Pimcore\Http\Request\Resolver\DocumentResolver;
use Pimcore\Localization\LocaleServiceInterface;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Templating\Helper\PimcoreUrl;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class AbstractKitt3nPimcoreFormLinkGenerator implements LinkGeneratorInterface, SharedPropertiesInterface
{

    /**
     * @var string
     */
    protected $sProperty = 'page';

    /**
     * @var DocumentResolver
     */
    protected $documentResolver;
    /**
     * @var RequestStack
     */
    protected $requestStack;
    /**
     * @var PimcoreUrl
     */
    protected $pimcoreUrl;
    /**
     * @var LocaleServiceInterface
     */
    protected $localeService;

    /**
     * LinkGenerator constructor.
     *
     * @param DocumentResolver $documentResolver
     * @param RequestStack $requestStack
     * @param PimcoreUrl $pimcoreUrl
     * @param LocaleServiceInterface $localeService
     */
    public function __construct(
        DocumentResolver $documentResolver,
        RequestStack $requestStack,
        PimcoreUrl $pimcoreUrl,
        LocaleServiceInterface $localeService
    ) {
        $this->documentResolver = $documentResolver;
        $this->requestStack = $requestStack;
        $this->pimcoreUrl = $pimcoreUrl;
        $this->localeService = $localeService;
    }

    /**
     * @param Concrete $object
     * @param array    $params
     *
     * @return string
     */
    public function generate(Concrete $object, array $params = []): string
    {
        $request = $this->requestStack->getCurrentRequest();
        $fullPath = '';

        $oDocument = $this->documentResolver->getDocument($this->requestStack->getCurrentRequest());

        $oOverrideTargetDocument = $oDocument->getProperty($this->sPropertyPrefix() . "." . $this->sProperty);
        if (!empty($oOverrideTargetDocument)) {
            $oDocument = $oOverrideTargetDocument;
        }

        $oOverrideTargetDocument = $params['parameters']['document'] ?? $params['parameters']['document'];
        if (!empty($oOverrideTargetDocument)) {
            $oDocument = $oOverrideTargetDocument;
        }

        $localeUrlPart = '/' . $this->localeService->getLocale() . '/';

        /*
         * $oDocument->getFullPath() could be "/de" or "/" ("/en") e.g. if called via annotation route in controller
         *   Use $request->getRequestUri() instead of $oDocument->getFullPath()
         */
        if ($oDocument && $oDocument->getFullPath() !== rtrim($localeUrlPart,
                '/') && $oDocument->getFullPath() !== '/') {
            $fullPath = ltrim(str_replace($localeUrlPart, '', $oDocument->getFullPath()), '/');
        } else {
            $fullPath = ltrim(str_replace($localeUrlPart, '', $request->getRequestUri()), '/');
        }

        $url = $this->pimcoreUrl->__invoke(
            [
                'slug' => $object->getSlug(),
                'id' => $object->getId(),
                'path' => ($fullPath === '' ? 'f' : $fullPath)
            ],
            $this->sPropertyPrefix(),
            true
        );
        return str_replace('//', '/', $url);
    }

    /**
     * @return string
     */
    public function sParentMembersGroup(): string
    {
        return '__Kitt3nPimcoreForm';
    }
}
