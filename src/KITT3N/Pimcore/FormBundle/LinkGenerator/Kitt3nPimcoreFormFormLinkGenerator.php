<?php

namespace KITT3N\Pimcore\FormBundle\LinkGenerator;

use KITT3N\Pimcore\FormBundle\Traits\Kitt3nPimcoreFormFormTrait;
use Pimcore\Model\DataObject\Concrete;

class Kitt3nPimcoreFormFormLinkGenerator extends AbstractKitt3nPimcoreFormLinkGenerator
{

    use Kitt3nPimcoreFormFormTrait;

    /**
     * @param Concrete $object
     * @param array $params
     *
     * @return string
     */
    public function generate(Concrete $object, array $params = []): string
    {
        return parent::generate($object, $params);
    }
}
