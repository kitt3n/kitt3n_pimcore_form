<?php

namespace KITT3N\Pimcore\FormBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreFormBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcoreform/js/pimcore/startup.js'
        ];
    }
}