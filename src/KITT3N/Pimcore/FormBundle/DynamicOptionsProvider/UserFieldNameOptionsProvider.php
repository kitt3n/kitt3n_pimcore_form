<?php

namespace KITT3N\Pimcore\FormBundle\DynamicOptionsProvider;

use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;


class UserFieldNameOptionsProvider implements SelectOptionsProviderInterface
{
    /**
     * @param array $context
     * @param Data $fieldDefinition
     * @return array
     */
    public function getOptions($context, $fieldDefinition) {

        $aOptions = get_class_methods ( '\\Pimcore\\Model\\DataObject\\MembersUser' );
        $aAbstractOptions = get_class_methods ( '\\MembersBundle\\Adapter\\User\\AbstractUser');

        $return = [];
        foreach ($aOptions as $sOption) {
            if (substr($sOption, 0, 3) === 'get') {
                if (!in_array($sOption, $aAbstractOptions)) {
                    $return[] = [
                        'key' => ucfirst(str_replace('get', '', $sOption)),
                        'value' => ucfirst(str_replace('get', '', $sOption)),
                    ];
                }
            }
        }

        return $return;
    }

    /**
     * Returns the value which is defined in the 'Default value' field
     * @param array $context
     * @param Data $fieldDefinition
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition) {
        return $fieldDefinition->getDefaultValue();
    }

    /**
     * @param array $context
     * @param Data $fieldDefinition
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition) {
        return true;
    }

}
