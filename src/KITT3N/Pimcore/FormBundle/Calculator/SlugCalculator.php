<?php

namespace KITT3N\Pimcore\FormBundle\Calculator;

use Pimcore\Model\DataObject\Concrete;

class SlugCalculator
{
    /**
     * @param $object Concrete
     * @param $context \Pimcore\Model\DataObject\Data\CalculatedValue
     * @return string
     */
    public static function compute($object, $context) {
        if ($context->getFieldname() == "slug") {

            $sLanguage = $context->getPosition();

            return trim(
                preg_replace(
                    "![^a-z0-9]+!i",
                    "-",
                    strtolower(
                        trim(
                            preg_replace(
                                '/[^A-Za-z0-9-~]+/',
                                '-',
                                $object->getTitle($sLanguage)
                            )
                        )
                    )
                ),
                '-'
            );
        } else {
            \Logger::error("unknown field");
        }
    }
}
