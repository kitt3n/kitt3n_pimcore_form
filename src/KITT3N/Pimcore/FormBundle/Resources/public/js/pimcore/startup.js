pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreFormBundle");

pimcore.plugin.Kitt3nPimcoreFormBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreFormBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreFormBundle ready!");
    }
});

var Kitt3nPimcoreFormBundlePlugin = new pimcore.plugin.Kitt3nPimcoreFormBundle();
