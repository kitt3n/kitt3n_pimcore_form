<?php

namespace KITT3N\Pimcore\FormBundle\Traits;

trait Kitt3nPimcoreFormFormTrait {

    /**
     * @return string
     */
    public function sClass(): string
    {
        return 'Pimcore\\Model\\DataObject\\Kitt3nPimcoreFormForm';
    }

    /**
     * @return string
     */
    public function sPropertyPrefix(): string
    {
        return 'Kitt3nPimcoreFormForm';
    }

    /**
     * @return string
     */
    public function sMembersGroup(): string
    {
        return '__Kitt3nPimcoreFormForm';
    }

}
