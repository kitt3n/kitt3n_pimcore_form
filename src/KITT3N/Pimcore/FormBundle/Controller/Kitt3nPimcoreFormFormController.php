<?php

namespace KITT3N\Pimcore\FormBundle\Controller;

use KITT3N\Pimcore\FormBundle\Traits\Kitt3nPimcoreFormFormTrait;
use MembersBundle\Manager\RestrictionManager;
use Pimcore\Translation\Translator as SharedTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Kitt3nPimcoreFormFormController extends AbstractController
{

    use Kitt3nPimcoreFormFormTrait;

    /**
     * @Route(
     *     {
     *         "en": "/{path}/{slug}.form{id}",
     *         "de": "/{path}/{slug}.form{id}",
     *     },
     *     requirements={
     *         "_locale": "en|de",
     *         "slug": "[A-Za-z0-9-]+",
     *         "id": "\d+",
     *         "path": ".*?"
     *     },
     *     name="Kitt3nPimcoreFormForm"
     * )
     */
    public function detailAction(Request $request, RestrictionManager $restrictionManager, SharedTranslator $sharedTranslator)
    {
        parent::detailAction($request,$restrictionManager, $sharedTranslator);
    }
}
