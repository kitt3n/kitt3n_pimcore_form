<?php

namespace KITT3N\Pimcore\FormBundle\Controller;

use KITT3N\Pimcore\FormBundle\Controller\Interfaces\Kitt3nPimcoreFormBundleControllerInterface;
use KITT3N\Pimcore\MembersBundle\Controller\Traits\MembersTrait; //TODO Move to separate bundle
use KITT3N\Pimcore\FormBundle\Interfaces\SharedPropertiesInterface;
use MembersBundle\Manager\RestrictionManager;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject\Data\ElementMetadata;
use Pimcore\Translation\Translator as SharedTranslator;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Formatter\MessageFormatter;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints;

abstract class AbstractController extends FrontendController implements SharedPropertiesInterface, Kitt3nPimcoreFormBundleControllerInterface
{

    use ControllerTrait, MembersTrait;

    public function sParentMembersGroup(): string
    {
        return '__Kitt3nPimcoreForm';
    }

    public function restrictedDetailAction(Request $request, RestrictionManager $restrictionManager, SharedTranslator $sharedTranslator)
    {

        $this->redirectToLoginIfNotLoggedIn();

        $this->redirectToRestrictionErrorIfAccessIsNotGrantedForGivenGroup(
            $this->sParentMembersGroup(),
            $restrictionManager
        );

        $this->enableViewAutoRender($request, self::TWIG_ENGINE);

        $sLocale = $request->getLocale();
        if ( ! null == $sLocale) {
            $request->setLocale($sLocale);
        }

        $iId = intval($request->attributes->get('id'));

        /* @var string $sClass */
        $sClass = $this->sClass();

        /* @var \Pimcore\Model\DataObject\Concrete $oObject */
        $oObject = $sClass::getById($iId);

        if (!$oObject instanceof $sClass or !$oObject->isPublished()) {
            throw $this->createNotFoundException('Invalid request - no such object present.');
        }

        $this->redirectToRestrictionError(
            $oObject,
            $restrictionManager
        );

        $this->view->oObject = $oObject;

    }

    public function detailAction(
        Request $request,
        RestrictionManager $restrictionManager,
        SharedTranslator $sharedTranslator
    ) {

        $this->enableViewAutoRender($request, self::TWIG_ENGINE);

        $sLocale = $request->getLocale();
        if ( ! null == $sLocale) {
            $request->setLocale($sLocale);
        }

        $iId = intval($request->attributes->get('id'));

        /* @var string $sClass */
        $sClass = $this->sClass();

        /* @var \Pimcore\Model\DataObject\Concrete $oObject */
        $oObject = $sClass::getById($iId);

        if (!$oObject instanceof $sClass or !$oObject->isPublished()) {
            throw $this->createNotFoundException('Invalid request - no such object present.');
        }

        /* @var \Pimcore\Model\DataObject\Kitt3nPimcoreFormForm $oObject */
        $this->view->oObject = $oObject;

        if ($oObject->getRequiresLogin()) {

            $this->redirectToLoginIfNotLoggedIn();

            $this->redirectToRestrictionErrorIfAccessIsNotGrantedForGivenGroup(
                $this->sParentMembersGroup(),
                $restrictionManager
            );

            $this->redirectToRestrictionError(
                $oObject,
                $restrictionManager
            );
        }

        $this->renderForm($request, $sharedTranslator);

    }

    public function renderForm (
        Request $request,
        SharedTranslator $sharedTranslator
    ) {

        $oObject = $this->view->oObject;
        $isValide = false;

        if ($oObject instanceof \Pimcore\Model\DataObject\Kitt3nPimcoreFormForm) {

            $oRelatedObject = null;
            $oMembersUser = $this->getUser();

            /*
             * $requestParameters['kpf']['o']
             *     id of the current object with a form
             * $requestParameters['kpf']['g']
             *     form fieldname in the the current object with a form
             *
             * If both parameters are set we check if the object kpf[o]
             * has a getter method for the given field kpf[g] and
             * if the linked form equals the current form.
             */
            $requestParameters = array_merge($request->request->all(), $request->query->all());
            if (isset($requestParameters['kpf']['o']) && !isset($requestParameters['kpf']['g'])) {
                throw $this->createNotFoundException(__LINE__ . ':: Invalid request.');
            }
            if (!isset($requestParameters['kpf']['o']) && isset($requestParameters['kpf']['g'])) {
                throw $this->createNotFoundException(__LINE__ . ':: Invalid request.');
            }
            if (isset($requestParameters['kpf']['o']) && isset($requestParameters['kpf']['g'])) {

                if ((int)$requestParameters['kpf']['o'] == -99 && (int)$requestParameters['kpf']['g'] == -99) {

                    // No related object

                } else {

                    $oRelatedObject = \Pimcore\Model\DataObject::getById(intval($requestParameters['kpf']['o']));
                    $sFormMethod = 'get' . ucfirst($requestParameters['kpf']['g']);
                    if(!method_exists($oRelatedObject, $sFormMethod)) {
                        //exception
                        throw $this->createNotFoundException(__LINE__ . ':: Invalid request.');
                    }
                    if (empty($oRelatedObject->$sFormMethod()) or $oRelatedObject->$sFormMethod()->getId() !== $oObject->getId()) {
                        //exception
                        throw $this->createNotFoundException(__LINE__ . ':: Invalid request.');
                    }

                }

            }


            $aFields = [];
            $aElementMetaDataObjects = $oObject->getFields();

            /* @var ElementMetadata[] $aElementMetaDataObjects */
            foreach ($aElementMetaDataObjects as $oElementMetaData) {
                $aFields[$oElementMetaData->getElement()->getId()] = [
                    'data' => $oElementMetaData->getData(),
                    'element' => $oElementMetaData->getElement(),
                ];
            }

            $sLocale = $request->getLocale();
            if ( ! null == $sLocale) {
                $request->setLocale($sLocale);
            }

            /*
             * Translator for validator messages
             */
            $translator = new Translator($sLocale, new MessageFormatter());

            $vendorDir = $_SERVER['DOCUMENT_ROOT'] . '/../vendor';
            $vendorFormDir = $vendorDir.'/symfony/symfony/src/Symfony/Component/Form';
            $vendorValidatorDir = $vendorDir.'/symfony/symfony/src/Symfony/Component/Validator';

            $translator->addLoader('xlf', new XliffFileLoader());
            // there are built-in translations for the core error messages
            $translator->addResource('xlf', $vendorFormDir.'/Resources/translations/validators.en.xlf', 'en', 'validators');
            $translator->addResource('xlf', $vendorFormDir.'/Resources/translations/validators.de.xlf', 'de', 'validators');
            $translator->addResource('xlf', $vendorValidatorDir.'/Resources/translations/validators.en.xlf', 'en', 'validators');
            $translator->addResource('xlf', $vendorValidatorDir.'/Resources/translations/validators.de.xlf', 'de', 'validators');

            $validator = Validation::createValidatorBuilder()
                ->setTranslator($translator)
                ->setTranslationDomain('validators')
                ->getValidator();

            $form = Forms::createFormFactoryBuilder()
                ->addExtension(
                    new ValidatorExtension(
                        $validator
                    )
                )
                ->getFormFactory()
                ->createNamedBuilder(
                    'kpf_' . $oObject->getId(),
                    Type\FormType::class,
                    [])
                ->setMethod('POST')
                ->getForm();

            foreach ($aFields as $aField) {

                $oFieldElement = $aField['element'];

                $aOptions = [];

//                $aOptions['constraints'] = [
//                    new Constraints\Regex([
//                        'pattern' => '/\d/',
//                        'match' => true,
//                    ]),
//                ];

                $aOptions['required'] = false;
                if ($aField['data']['mandatory']) {
                    $aOptions['required'] = true;
                    $aOptions['constraints'] = [new Constraints\NotBlank()];
                    $aOptions['label'] = $oFieldElement->getLabel();
                    //$aOptions['help'] = 'Mandatory field';
                }

                switch (true) {
                    case $aField['element'] instanceof \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldInput:

                        /* @var \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldInput $oFieldElement */
                        $aOptions['label'] = (isset($aOptions['label']) ? $aOptions['label'] : $oFieldElement->getLabel());

                        $aOptions['attr'] = [
                            'placeholder' => $aField['element']->getPlaceholder(),
                        ];

                        $aOptions['label'] = strip_tags($aOptions['label']);

                        /*
                         * Prefill with data from current user
                         */
                        if ($aField['element']->getUserFieldName() != '') {
                            if (!empty($oMembersUser) and $oMembersUser instanceof \Pimcore\Model\DataObject\MembersUser) {
                                $getMethod = 'get' . ucfirst($aField['element']->getUserFieldName());
                                if (method_exists($oMembersUser, $getMethod)) {
                                    if (!empty($oMembersUser->$getMethod())) {
                                        $aOptions['empty_data'] = $oMembersUser->$getMethod();
                                        $aOptions['data'] = $oMembersUser->$getMethod();
                                        $aOptions['attr']['readonly'] = true;
                                    }
                                }
                            }
                        }

                        $form->add(
                            $aField['element']->getId(),
                            Type\TextType::class,
                            $aOptions
                        );

                        break;
                    case $aField['element'] instanceof \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldTextarea:

                        /* @var \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldTextarea $oFieldElement */
                        $aOptions['label'] = (isset($aOptions['label']) ? $aOptions['label'] : $oFieldElement->getLabel());

                        $aOptions['label'] = strip_tags($aOptions['label']);

                        $form->add(
                            $aField['element']->getId(),
                            Type\TextareaType::class,
                            $aOptions
                        );

                        break;
                    case $aField['element'] instanceof \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldSelect:

                        /* @var \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldSelect $oFieldElement */
                        $aShowAs = explode(",", $oFieldElement->getShowAs());

                        $aOptions['label'] = (isset($aOptions['label']) ? $aOptions['label'] : $oFieldElement->getLabel());
                        $aOptions['expanded'] = boolval(explode("=", $aShowAs[0] )[1]);
                        $aOptions['multiple'] = boolval(explode("=", $aShowAs[1] )[1]);

                        $aElementOptions = $oFieldElement->getOptions();
                        $aChoices = [];
                        foreach ($aElementOptions as $aOption) {

                            $aChoices[$aOption['label']] = $aOption['value'];

                            $aOverrideDefaultValues = explode(",", $aField['data']['defaultValue']);
                            if ($aOverrideDefaultValues[0] !== '' && in_array($aOption['value'], $aOverrideDefaultValues)) {
                                if (!$form->isSubmitted()) {
                                    switch (true) {
                                        case $aOptions['multiple']:
                                            $aOptions['data'][] = $aOption['value'];
                                            break;
                                        default:
                                            $aOptions['data'] = $aOption['value'];
                                    }
                                }
                            }

                            if ($aOverrideDefaultValues[0] === '' && $aOption['default'] === '1') {
                                if (!$form->isSubmitted()) {
                                    //$aOptions['empty_data'] = $aOption['value'];
                                    switch (true) {
                                        case $aOptions['multiple']:
                                            $aOptions['data'][] = $aOption['value'];
                                            break;
                                        default:
                                            $aOptions['data'] = $aOption['value'];
                                    }
                                }
                            }

                        }
                        $aOptions['choices'] = $aChoices;

                        $aOptions['label'] = strip_tags($aOptions['label']);

                        $form->add(
                            $aField['element']->getId(),
                            Type\ChoiceType::class,
                            $aOptions
                        );

                        break;
                    case $aField['element'] instanceof \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldCountry:

                        /* @var \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldCountry $oFieldElement */
                        $aOptions['label'] = (isset($aOptions['label']) ? $aOptions['label'] : $oFieldElement->getLabel());

                        if ($aField['data']['defaultValue'] !== '') {
                            if (!$form->isSubmitted()) {
                                $aOptions['data'] = $aField['data']['defaultValue'];
                            }
                        }

                        $aOptions['label'] = strip_tags($aOptions['label']);

                        $form->add(
                            $aField['element']->getId(),
                            Type\CountryType::class,
                            $aOptions
                        );

                        break;
                    case $aField['element'] instanceof \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldCheckbox:

                        /* @var \Pimcore\Model\DataObject\Kitt3nPimcoreFormFieldCheckbox $oFieldElement */
                        $aOptions['label'] = (isset($aOptions['label']) ? $aOptions['label'] : $oFieldElement->getLabel());

                        if ($aField['data']['defaultValue'] !== '') {
                            if (!$form->isSubmitted()) {
                                $aOptions['data'] = boolval($aField['data']['defaultValue']);
                            }
                        }

                        if ($aField['data']['defaultValue'] === '' && $oFieldElement->getDefaultValue() !== '') {
                            if (!$form->isSubmitted()) {
                                $aOptions['data'] = boolval($aField['data']['defaultValue']);
                            }
                        }

                        $form->add(
                            $aField['element']->getId(),
                            Type\CheckboxType::class,
                            $aOptions
                        );

                        break;
                }
            }

            $form->add($oObject->getSubmitLabel(), Type\SubmitType::class);

            $form->handleRequest();
            if ($form->isSubmitted()) {
                if ($form->isValid()) {

                    $newObject = new \Pimcore\Model\DataObject\Kitt3nPimcoreFormSubmission();
                    $newObject->setKey(
                        $oObject->getId() . '_' .
                        md5(
                            time() .
                            rand(
                                100000000000,
                                999999999999
                            )
                        )
                    );
                    $oSubmissionFolder = \Pimcore\Model\WebsiteSetting::getByName('KITT3N\\Pimcore\\FormBundle\\Controller\\AbstractController.renderForm.submissionFolder');
                    if(!empty($oSubmissionFolder)) {
                        $newObject->setParentId($oSubmissionFolder->getData()->getId());
                    } else {
                        $newObject->setParentId(1);
                    }

                    if (!empty($oMembersUser) and $oMembersUser instanceof \Pimcore\Model\DataObject\MembersUser) {
                        $newObject->setMembersUser($oMembersUser);
                    }
                    if (!empty($oRelatedObject) and $oRelatedObject instanceof \Pimcore\Model\DataObject\Concrete) {
                        $newObject->setRelatedObject($oRelatedObject);
                    }
                    $newObject->setSubmittedForm($oObject);
                    $aData = $form->getData();
                    $sHeadline = '';
                    $sData = '';
                    foreach ($aData as $key => $value) {
                        $sHeadline .= "`" . $aFields[$key]['element']->getTitle() . "`|";
                        $sData .= "`" . implode(', ', (array)(str_replace(PHP_EOL, '\r\n', $value))) . "`|";
                    }
                    $newObject->setSubmittedValues($sHeadline . PHP_EOL . $sData);
                    $newObject->setPublished(true);
                    $newObject->save();

                    $isValide = true;

                }
            }
            $this->view->form = $form->createView();
            $this->view->isValide = $isValide;

        }

    }

}
