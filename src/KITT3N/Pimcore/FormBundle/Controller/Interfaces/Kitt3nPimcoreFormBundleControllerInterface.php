<?php

namespace KITT3N\Pimcore\FormBundle\Controller\Interfaces;

interface Kitt3nPimcoreFormBundleControllerInterface {

    public const TWIG_ENGINE = 'twig';

    public const PHP_ENGINE = 'php';

}
